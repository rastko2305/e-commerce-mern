import { 
  PRODUCT_LIST_REQUEST, 
  PRODUCT_LIST_SUCCESS, 
  PRODUCT_LIST_ERROR,
} from "../../constants/productConstants";

export const productReducer = (state = {loading: false, error: false, products: []}, action) => {
  if (action.type === PRODUCT_LIST_REQUEST) {
    return {loading: true, error: false, products: []};
  }
  else if (action.type === PRODUCT_LIST_SUCCESS) {
    return { loading: false, error: false, products: action.payload} ;
  }
  else if (action.type === PRODUCT_LIST_ERROR) {
    return { loading: false, error: action.payload, products: []};
  }
  else {
    return state;
  }
}