import { 
  PRODUCT_DETAIL_REQUEST, 
  PRODUCT_DETAIL_SUCCESS, 
  PRODUCT_DETAIL_ERROR,
} from "../../constants/productConstants";

export const productDetailReducer = (state = {loading: false, error: false, product: undefined }, action) => {
  if (action.type === PRODUCT_DETAIL_REQUEST) {
    return {loading: true};
  }
  else if (action.type === PRODUCT_DETAIL_SUCCESS) {
    return { loading: false, product: action.payload};
  }
  else if (action.type === PRODUCT_DETAIL_ERROR) {
    return { loading: false, error: action.payload};
  }
  else {
    return state;
  }
}