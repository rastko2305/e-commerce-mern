import { CART_ADD_ITEM , CART_DELETE_ITEM} from "../../constants/cartConstants"
import { USER_SIGN_OUT } from "../../constants/userConstants";

export const cartReducer = (state = {cartItems: []}, action) => {
  if (action.type === CART_ADD_ITEM) {
    // product should be added only if not in the cart or changed if there is new quantity
    const product = action.payload;
    if( state.cartItems.some(item => item.productId === product.productId)) {
      return {
        ...state,
        cartItems: state.cartItems.map(item => item.productId === product.productId ? product : item ),
      };
    }
    else {
      return { 
        ...state, 
        cartItems: [...state.cartItems, product]
      }
    }
  }
  else if (action.type === USER_SIGN_OUT) {
    return {...state, cartItems: []}
  }
  else if(action.type === CART_DELETE_ITEM) {
    return {
      ...state,
      cartItems: state.cartItems.filter((item) => item.productId !== action.payload),
    };
  }
  else {
    return state;
  }
}





