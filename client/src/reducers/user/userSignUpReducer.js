import { USER_SIGN_UP_REQUEST , USER_SIGN_UP_SUCCESS, USER_SIGN_UP_ERROR } from "../../constants/userConstants"


export const userSignUpReducer = (state = {}, action) => {
  if (action.type === USER_SIGN_UP_REQUEST) {
    return {loading: true}
  }
  else if (action.type === USER_SIGN_UP_SUCCESS) {
    return {loading: false }
  }
  else if (action.type === USER_SIGN_UP_ERROR) {
    return {loading: false, error: action.payload}
  }
  else {
    return state;
  }
}