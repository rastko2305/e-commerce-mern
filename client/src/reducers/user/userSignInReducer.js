import { USER_SIGN_IN_REQUEST , USER_SIGN_IN_SUCCESS, USER_SIGN_IN_ERROR, USER_SIGN_OUT} from "../../constants/userConstants"


export const userSignInReducer = (state = {}, action) => {
  if (action.type === USER_SIGN_IN_REQUEST) {
    return {loading: true}
  }
  else if (action.type === USER_SIGN_IN_SUCCESS) {
    return {loading: false, userData: action.payload}
  }
  else if (action.type === USER_SIGN_IN_ERROR) {
    return {loading: false, error: action.payload}
  }
  else if (action.type === USER_SIGN_OUT) {
    return {}
  }
  else {
    return state;
  }
}