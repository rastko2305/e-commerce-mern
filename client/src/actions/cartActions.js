import Axios from "axios";
import {CART_ADD_ITEM, CART_DELETE_ITEM} from '../constants/cartConstants';

export const addToCart = (id, quantity) => async(dispatch, getState) => {
  const { data } = await Axios.get(`http://localhost:8000/api/products/${id}`);
  dispatch({
    type: CART_ADD_ITEM,
    payload: {
      name: data.name,
      image: data.image,
      price: data.price,
      productId: id,
      quantity,
      numberOfReviews: data.numberOfItemsInStock,
      rating: data.rating,
      numberOfItemsInStock: data.numberOfItemsInStock
    }
  })
  localStorage.setItem("cartItems", JSON.stringify(getState().cart.cartItems));
};

export const deleteFromCart  = (id) => (dispatch, getState) => {
  dispatch({
    type: CART_DELETE_ITEM,
    payload: id
  });
  localStorage.setItem("cartItems", JSON.stringify(getState().cart.cartItems));
}