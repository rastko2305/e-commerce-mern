import {
  PRODUCT_LIST_REQUEST, 
  PRODUCT_LIST_SUCCESS, 
  PRODUCT_LIST_ERROR, 
  PRODUCT_DETAIL_REQUEST,
  PRODUCT_DETAIL_SUCCESS,
  PRODUCT_DETAIL_ERROR
} from '../constants/productConstants';
import Axios from "axios";


export const fetchProducts = () => async (dispatch) => {
  dispatch({
    type: PRODUCT_LIST_REQUEST
  });
  try {
    const { data }  = await Axios.get("http://localhost:8000/api/products");
    dispatch({type: PRODUCT_LIST_SUCCESS, payload: data});
  }
  catch(error) {
    dispatch({type: PRODUCT_LIST_ERROR, payload: error.message});
  }
}

export const fetchProductDetail = (id) => async (dispatch) => {
  dispatch({ type: PRODUCT_DETAIL_REQUEST, payload: id });
  try {
    const { data } = await Axios.get(`http://localhost:8000/api/products/${id}`);
    dispatch({ type: PRODUCT_DETAIL_SUCCESS, payload: data });
  } catch (error) {
    dispatch({
      type: PRODUCT_DETAIL_ERROR,
      payload:
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message,
    });
  }
};