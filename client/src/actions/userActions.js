import {
  USER_SIGN_IN_REQUEST, 
  USER_SIGN_IN_ERROR, 
  USER_SIGN_IN_SUCCESS, 
  USER_SIGN_OUT, USER_SIGN_UP_REQUEST, 
  USER_SIGN_UP_SUCCESS, 
  USER_SIGN_UP_ERROR
} from '../constants/userConstants';
import axios from "axios";

export const signIn = (email, password) => async (dispatch) => {
  dispatch({type: USER_SIGN_IN_REQUEST, payload: {email, password}});
  try {
    const {data} = await axios.post("/api/users/signin", {email, password});
    dispatch({type: USER_SIGN_IN_SUCCESS, payload: data});
    localStorage.setItem("userData", JSON.stringify(data));
  } catch (error) {
    dispatch({
      type: USER_SIGN_IN_ERROR,
      payload: error.response && error.response.data.message
      ? error.response.data.message
      : error.message
    })
  }
};

export const signOut = () => (dispatch) => {
  localStorage.removeItem("userData");
  localStorage.removeItem("cartItems");
  dispatch(
    {type: USER_SIGN_OUT}
  )
};

export const signUp = (name, email, password) => async (dispatch) => {
  dispatch({type: USER_SIGN_UP_REQUEST, payload: {name,email, password}});
  try {
    const {data} = await axios.post("/api/users/signup", {name, email, password});
    dispatch({type: USER_SIGN_UP_SUCCESS });
    dispatch({type: USER_SIGN_IN_SUCCESS, payload: data});
    localStorage.setItem("userData", JSON.stringify(data));
  } catch (error) {
    dispatch({
      type: USER_SIGN_UP_ERROR,
      payload: error.response && error.response.data.message
      ? error.response.data.message
      : error.message
    })
  }
};