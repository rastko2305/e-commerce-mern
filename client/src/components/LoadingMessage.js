import React from "react";

const LoadingMessage = () => {
  return (
    <div>
       <i className="fa fa-spinner fa-spin" ></i>Loading...
    </div>
  );
};

export default LoadingMessage;