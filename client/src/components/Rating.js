import React from 'react';

const starStyle = {color: "orange"};

function Rating(props) {
  const { rating, numberOfReviews } = props;

  return (
    <div className="rating">
      <span style={starStyle}>
      <i
          className={
            rating >= 1.0
              ? 'fa fa-star'
              : rating >= 0.5
              ? 'fa fa-star-half-o'
              : 'fa fa-star-o'
          }
        ></i>
      </span>
      <span style={starStyle}>
      <i
          className={
            rating >= 2.0
              ? 'fa fa-star'
              : rating >= 1.5
              ? 'fa fa-star-half-o'
              : 'fa fa-star-o'
          }
        ></i>
      </span>
      <span style={starStyle}>
      <i
          className={
            rating >= 2.0
              ? 'fa fa-star'
              : rating >= 2.5
              ? 'fa fa-star-half-o'
              : 'fa fa-star-o'
          }
        ></i>
      </span>
      <span style={starStyle}>
      <i
          className={
            rating >= 3.0
              ? 'fa fa-star'
              : rating >= 3.5
              ? 'fa fa-star-half-o'
              : 'fa fa-star-o'
          }
        ></i>
      </span>
      <span style={starStyle}>
      <i
          className={
            rating >= 4.0
              ? 'fa fa-star'
              : rating >= 4.5
              ? 'fa fa-star-half-o'
              : 'fa fa-star-o'
          }
        ></i>
      </span>
      <span>{numberOfReviews + ' reviews'}</span>
  </div>
  );
}

export default Rating;