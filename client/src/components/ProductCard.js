import React from "react";
import { Link } from "react-router-dom";
import Rating from "./Rating";

const ProductCard  = (props) => {
  return (
    <div className="card">
      {/* think about making some standard for image size for easier scaling */}
      <Link to={`/product/${props.product._id}`}>
        <img src={props.product.image}  className="medium" alt={props.product.name} />
      </Link>
      <div className="card-body">
        <Link to={`/product/${props.product._id}`}>
          <h2>{props.product.name}</h2>
        </Link>
        <Rating 
          rating={props.product.rating} 
          numberOfReviews={props.product.numberOfReviews}
        />
        <div className="price">
          {`$${props.product.price}`}
        </div>
      </div>
    </div>
  )
}

export default ProductCard;
