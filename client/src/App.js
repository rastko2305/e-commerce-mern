import "./App.css";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import ProductPage from "./pages/ProductPage";
import HomePage from "./pages/HomePage";
import CartPage from './pages/CartPage'
import { useSelector, useDispatch } from "react-redux";
import SignInPage from "./pages/SignInPage";
import { signOut } from "./actions/userActions";
import SignUpPage from "./pages/SignUpPage";


const App = ()  => {
  const numberOfCartItems = useSelector(state => state.cart.cartItems).length;
  const { userData } = useSelector(state => state.userSignIn);
  const dispatch = useDispatch();

  const signOutHandler = () => {
    dispatch (signOut ());
  }

  return (
    <Router>
      <div className="App">
        <div className="grid-container">
          <header className="row">
            <div>
              <Link className="brand" to="/">
                Just Shop
                <h5>This is not just another shop this is the Just Shop</h5>
              </Link>
            </div>
            <div>
              {/* Fix next line because a link is used unnecesseraly*/}
              {userData ? <Link>{userData.name}</Link> : <></>}
              <Link to="/cart">{`Cart (${numberOfCartItems} items)`}</Link>
              { userData ? 
              <Link to="/" onClick={signOutHandler}>Sign Out</Link>
              :
              <Link to="/signin">Sign In</Link>
              }             
            </div>
          </header>
          <main>
            <Route path="/product/:id" component={ProductPage}></Route>
            <Route path="/" component={HomePage} exact></Route>
            <Route path="/cart/:id?" component={CartPage}></Route>
            <Route path="/signin" component={SignInPage}></Route>
            <Route path="/signup" component={SignUpPage}></Route>
          </main>
          <footer className="row center">All right reserved</footer>
        </div>
      </div>
    </Router>
  );
}

export default App;

