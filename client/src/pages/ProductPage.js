import React, { useEffect, useState } from "react";
import { useDispatch, useSelector} from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { fetchProductDetail } from "../actions/productActions";
import Rating from "../components/Rating";
import LoadingMessage from "../components/LoadingMessage"
import Message from "../components/Message";

const style ={border: "solid black 1px" };

const ProductPage = (props) => {

  const dispatch = useDispatch();
  const history = useHistory();
  const productId = props.match.params.id;
  let [quantity, setQuantity] = useState(1);
  const {error, loading, product} = useSelector((state) => state.productDetail);

  useEffect(() => {
    dispatch(fetchProductDetail(productId));
  }, [dispatch, productId]);

  const addToCartHandler = (productId, quantity) => {
    history.push(`/cart/?pid=${productId}?qty=${quantity}`);
  }

    return (   
      <>
      {
      loading ? <LoadingMessage />
      :
      error ? <Message error={error}/>
      :
      product && 
      <div>
        <Link to="/">
            <p style={style}>Back</p>
        </Link>
        <div style={style}>
          <img src={product.image ? product.image : ""} alt={product.name}/>
        </div>
        <div className="" style={style}>
          <Rating 
            rating={product.rating} 
            numberOfReviews={product.numberOfReviews}
          />
          <p>{`Price: $${product.price}`}</p>
          <p>Description</p>
          <p>{product.productDescription}</p>
        </div>
        < AddToCartBox 
          product={product} 
          setQuantity={setQuantity}
          quantity={quantity}
          addToCartHandler={addToCartHandler}
        />
      </div>
      } 
    </>
  );
}

const AddToCartBox = (props) => {
  const {product, setQuantity, quantity, addToCartHandler } = props;
  console.log(product)
  return(
    <div style={style}>
    <ul>
      <li>
        <div>
          <p>Price</p>
        </div>
        <div>
          <p>{`$${product.price}`}</p>
        </div>
      </li>
      <li>
        <div>
          <p>Status</p>
        </div>
        <div>
          <p>{product.status}</p>
        </div>    
      </li>
      { (product.numberOfItemsInStock > 0 &&  product.status !== "Not Available")  &&
      <>
        <li>
          <p>
            Quantity
          </p>
          <div>
            <select
              value={quantity}
              onChange={(e) => setQuantity(e.target.value)}
            >
              {Array.from({length: product.numberOfItemsInStock}, (_, i) => i + 1).map(number => <option key={number} value={number}>{number}</option>)}
            </select>
          </div>
        </li>   
        <li>
          <button onClick={() => addToCartHandler(product._id, quantity)}>Add to Cart</button>
        </li>
      </>
      }
    </ul>
  </div>
  )
};

export default ProductPage;
