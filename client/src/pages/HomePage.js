import React, {useEffect} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { fetchProducts } from '../actions/productActions';
import ProductCard from "../components/ProductCard";
import LoadingMessage from "../components/LoadingMessage";
import Message from "../components/Message";

const HomePage = () =>  {
    const dispatch = useDispatch();
    const productList = useSelector( state => state.productList);
    const {loading, error, products} = productList;

    useEffect(() => {
      dispatch(fetchProducts());
    }, [dispatch]);

    return (
      <>
        {loading ? <LoadingMessage />
        :
        error ? <Message text={error}/>
        :
        <div className="row center">
              {products.map(product => <ProductCard product={product} key={product._id}/>)}
        </div>
        }   
      </>
    );
}

export default HomePage;