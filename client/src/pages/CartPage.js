import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector  } from 'react-redux';
import { addToCart, deleteFromCart } from "../actions/cartActions";
import Rating from "../components/Rating";
import {numberOfItems, totalPrice} from '../util/util';
import Message from "../components/Message";

const style = {border: "solid 1px black"}

const CartPage = (props) => {
  const dispatch = useDispatch();
  const [productId, quantity] = props.location.search
  ? props.location.search.split('?pid=')[1].split('?qty=')
  : [null, 1];

  const cartItems = useSelector(state => state.cart.cartItems);


  useEffect(() => {
    dispatch(addToCart(productId, quantity));
  }, [dispatch, productId, quantity]);

  const handleSelectChange = (newProductId, newQuantity) => {
    dispatch(addToCart(newProductId, newQuantity));
  }

  const deleteFromCartHandler = (productId) => {
    dispatch(deleteFromCart(productId));
  }

  return (
    <div>
      <div>
        <h1>Cart</h1>
        <>
          <Link to="/">Back to shopping</Link>
        </>
        {cartItems.length === 0 ? 
        <>
         {/* Cart is empty */}
          <Message text={"Cart is empty!"}></Message>
        </>
        :
        <ul>
          {cartItems.map(item => 
            <li key={item.productId} style={style}>
              <div>
                <div>
                  <img src={item.image} alt={item.name}/>
                </div>
                <div>
                  <Link to={`/product/${item.productId}`}>{item.name}</Link>
                  <Rating rating={item.rating} numberOfReviews={item.numberOfReviews}/>
                </div>
                <div>
                  <select
                    value={item.quantity}
                    onChange={(e) => handleSelectChange(item.productId, e.target.value)}
                  >
                    {Array.from({length: item.numberOfItemsInStock}, (_, i) => i + 1).map(number => <option key={number} value={number}>{number}</option>)}
                  </select>
                </div>
                <div>
                  {`$${item.price}`}
                </div>
                <div>
                  <button onClick={() => deleteFromCartHandler(item.productId)}>
                    Delete
                  </button>
                </div>
              </div>
            </li>
          )}
        </ul>
        }
      </div>
      {/* Create subtotal boc component */}
      { cartItems.length > 0 &&
      <div style={style}>
        <h2>subtotal box</h2>
        <h2>{cartItems.length} products</h2>
        <h2>{numberOfItems(cartItems)} items</h2>
        <h2>${totalPrice(cartItems)} </h2>
        <button>
          Checkout
        </button>
      </div>
      }   
    </div>
  );
};

export default CartPage;