import React, { useState, useEffect, useRef } from 'react';
import _ from "lodash/fp";
import { useForm } from "react-hook-form";
import { Link, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import {signUp} from '../actions/userActions';
import LoadingMessage from '../components/LoadingMessage';
import Message from '../components/Message';

const SignUpPage = (props) => {
  const { loading, error } = useSelector(state => state.userSignUp);
  const { userData } = useSelector(state => state.userSignIn);
  const [errorMessage, setErrorMessage ] = useState(null);
  const history = useHistory();
  const dispatch = useDispatch();
  const { register, handleSubmit, watch, setValue, errors } = useForm();
  const password = useRef({});
  password.current = watch("password", "");
  
  const onSubmit = (data) => {
      //alert(JSON.stringify(data))
      dispatch(signUp(data.name, data.email, data.password));
  }
  
  useEffect(() => {
    if(userData) {
      history.push("/");
    }
  })

  useEffect(() => {
    if(error) {
      setValue("name", null);
      setValue("email", null);
      setValue("password", null);
      setValue("confirmedPassword", null);
    }
  });


  return (
    <div>
      <div>
        {loading && <LoadingMessage></LoadingMessage>}
        {error && <Message text={error}></Message>}
        {!error && errorMessage && <Message text={errorMessage}></Message>}
      </div>
      <div>
        <form onSubmit={handleSubmit(onSubmit)}>
          <label>*</label>
          <input
            name="name"
            placeholder="Enter your name!"
            ref={register({
              required: true,
              maxLength: 30,
              pattern: /^[a-zA-Z]+$/i
            })}
          />
          {_.get("name.type", errors) === "required" && (
            <p style={{color: "red"}}>This field is required!</p>
          )}
          {_.get("name.type", errors) === "maxLength" && (
            <p style={{color: "red"}}>Name cannot exceed 30 characters!</p>
          )}
          {_.get("name.type", errors) === "pattern" && (
            <p style={{color: "red"}}>Name consists from letters only!</p>
          )}
          <br />
          <label>*</label>
          <input
            name="email"
            placeholder="Enter email!"
            ref={register({
              required: true,
              maxLength: 100,
              pattern: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/i
            })}
          />
          {_.get("email.type", errors) === "required" && (
            <p style={{color: "red"}}>This field is required!</p>
          )}
          {_.get("email.type", errors) === "maxLength" && (
            <p style={{color: "red"}}>Email name cannot exceed 100 characters!</p>
          )}
          {_.get("email.type", errors) === "pattern" && (
            <p style={{color: "red"}}>Entered value must be in email format!</p>
          )}
          <br />
          <label>*</label>
          <input 
          name="password"
          type="password" 
          placeholder="Enter your password!"
          ref={register({ 
            required: true,
            maxLength: 20,
            minLength: 6,
            pattern: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/i 
          })} 
          />
          {_.get("password.type", errors) === "required" && (
            <p style={{color: "red"}}>This field is required!</p>
          )}
          {_.get("password.type", errors) === "maxLength" && (
            <p style={{color: "red"}}>Password must not exceed 20 characters!</p>
          )}
          {_.get("password.type", errors) === "minLength" && (
            <p style={{color: "red"}}>Password must not deceed 6 characters!</p>
          )}
          {_.get("password.type", errors) === "pattern" && (
            <p style={{color: "red"}}>
              Password must contain at least one digit <br />
              and one letter!
            </p>
          )}
          <br />
          <label>*</label>
          <input
            name="confirmedPassword"
            placeholder="Confirm password!"
            type="password"
            ref={register({
              validate: value =>
                value === password.current || "Entered passwords must match!"
            })}
          />
          {errors.confirmedPassword && <p style={{color: "red"}}>{errors.confirmedPassword.message}</p>}
          <br />
          <input type="submit" />
          </form>
      </div>
      <div>
          Already have an account? {" "} <Link to="/signin">Sign In!</Link>
      </div>
    </div>
  );
}

export default SignUpPage;