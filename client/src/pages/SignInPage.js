import React, { useEffect } from "react";
import _ from "lodash/fp";
import { useForm } from "react-hook-form";
import { Link, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {signIn} from "../actions/userActions";
import LoadingMessage from "../components/LoadingMessage";
import Message from "../components/Message";

const SignInPage = (props) => {

  const { register, handleSubmit, watch, errors } = useForm();
  const { userData, loading, error } = useSelector(state => state.userSignIn);
  const history = useHistory();
  const dispatch = useDispatch();
  
  const onSubmit = data => {
    dispatch(signIn(data.email, data.password))
  };
  
  useEffect(() => {
    if(userData) {
      history.push("/");
    }
  })

  return (
    <div>
      <div>
        {loading && <LoadingMessage></LoadingMessage>}
        {error && <Message text={error}></Message>}
      </div>
      <div>
        <form onSubmit={handleSubmit(onSubmit)}>
        {/* <label>Email</label> */}
        <input
          name="email"
          placeholder="Enter email!"
          ref={register({
            required: true,
            maxLength: 100,
            pattern: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/i
          })}
        />
        {_.get("email.type", errors) === "required" && (
          <p style={{color: "red"}}>This field is required!</p>
        )}
        {_.get("email.type", errors) === "maxLength" && (
          <p style={{color: "red"}}>Email name cannot exceed 100 characters!</p>
        )}
        {_.get("email.type", errors) === "pattern" && (
          <p style={{color: "red"}}>Entered value must be in email format!</p>
        )}
        <br />
        {/* <label>Password</label> */}
        <input 
        name="password"
        type="password" 
        placeholder="Enter your password!"
        ref={register({ 
          required: true,
          maxLength: 20,
          minLength: 6,
          pattern: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/i 
        })} 
        />
        {_.get("password.type", errors) === "required" && (
          <p style={{color: "red"}}>This field is required!</p>
        )}
        {_.get("password.type", errors) === "maxLength" && (
          <p style={{color: "red"}}>Password must not exceed 20 characters!</p>
        )}
        {_.get("password.type", errors) === "minLength" && (
          <p style={{color: "red"}}>Password must not deceed 6 characters!</p>
        )}
        {_.get("password.type", errors) === "pattern" && (
          <p style={{color: "red"}}>
            Password must contain at least one digit <br />
            and one letter!
          </p>
        )}
        <br />
        <input type="submit" />
        </form>
      </div>
      <div>
          New customer? {" "} <Link to="/signup">Create an account!</Link>
      </div>
    </div>
  );
}

export default SignInPage;