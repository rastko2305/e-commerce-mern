import { createStore, compose,applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk";
import {productReducer} from "./reducers/product/productReducer";
import {productDetailReducer} from "./reducers/product/productDetailReducer";
import {cartReducer} from "./reducers/cart/cartReducer"
import {userSignInReducer} from './reducers/user/userSignInReducer';
import {userSignUpReducer} from './reducers/user/userSignUpReducer';


const initialState = {
  cart: {
    cartItems: localStorage.getItem("cartItems") ? JSON.parse(localStorage.getItem("cartItems")) : []
  },
  userSignIn: {
    userData: localStorage.getItem("userData") ? JSON.parse(localStorage.getItem("userData")) : null
  }
}

const reducer =  combineReducers ({
  productList: productReducer,
  productDetail: productDetailReducer,
  cart: cartReducer,
  userSignIn: userSignInReducer,
  userSignUp: userSignUpReducer
});

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  reducer,
  initialState,
  composeEnhancer(applyMiddleware(thunk))
);

export default store;