export const numberOfItems = (cartItems) => {
 return cartItems.reduce((acc, curr) => acc + Number(curr.quantity)  , 0);
};

export const totalPrice = (cartItems) => {
  const total = cartItems.reduce((acc, curr) => acc + Number(curr.quantity) * Number(curr.price)  , 0);
  return formatNumberToShowTwoDecimals(total);
}

export const formatNumberToShowTwoDecimals = (num) => {
 return Number(num).toFixed(2);   
}