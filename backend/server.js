import express from 'express';
import mongoose from "mongoose";
import cors  from "cors";
import userRouter from "./routes/userRouter";
import productRouter from './routes/productRouter';
import { config as dotenvConfig } from "dotenv";
import * as path from "path";

dotenvConfig();


const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect(process.env.MONGODB_URL || "mongodb://localhost/mern-e-commerce", {
  useNewUrlParser:true,
  useUnifiedTopology: true,
  useCreateIndex: true
});

app.use(cors());

const PORT = process.env.PORT || 8080;

app.use("/api/users", userRouter);
app.use("/api/products", productRouter);

// app.get("/", (req, res) => {
//   res.send("Server is on");
// });

/************************************************************************/
/*             THIS CODE IS USED FOR PUBLISHING APP TO HEROKU           */
/************************************************************************/
const __dirname = path.resolve();
app.use("/uploads", express.static(path.join(__dirname, "/uploads")));
app.use(express.static(path.join(__dirname, "/client/build")));
app.get("*", (res, req) => res.sendFile(path.join(__dirname, "/client/build.index/html")))
/************************************************************************/

app.use((err, req, res, next) => {
  res.status(500).send({ message: err.message });
});

app.listen (PORT, () => {
  console.log(`Server runs on port ${PORT}`);
});