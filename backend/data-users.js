const bcrypt = require("bcryptjs");

const users =  [
  {
    name: "Rastko",
    email: "rastko@mail.com",
    password: bcrypt.hashSync("password23", 8),
    isAdmin: true
  },
  {
    name: "Milos",
    email: "milos@mail.com",
    password: bcrypt.hashSync("password23", 8),
    isAdmin: false
  }
];

module.exports = users;