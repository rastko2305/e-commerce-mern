import express from "express";
import bcrypt from "bcryptjs";
import  users from "../data-users.js";
import User from "../models/userModel";
import { generateToken } from "../utils/util.js";

const userRouter = express.Router();

userRouter.get("/seed", async (req, res) => {
  //await User.remove({});
  const createdUsers = await User.insertMany(users);
  res.send({createdUsers});
})

userRouter.post("/signin", async (req, res) => {
    //console.log(req.body.email);
    const user = await User.findOne({email: req.body.email});
    if (user) {
      if(bcrypt.compareSync(req.body.password, user.password)) {
        res.status(200).send({
          _id: user._id,
          name: user.name,
          emai: user.email,
          isAdmin: user.isAdmin,
          token: generateToken(user)
        })
      }
      else {
        res.status(401).send({message: "Wrong password!"});
      }
    }
    else {
      res.status(401).send({message: `There is no user with email ${req.body.email}!`});
    }
  }
);

userRouter.post("/signup", async (req, res) => {
  const user = await User.findOne({email: req.body.email});
  if(user) {
    res.status(409).send({message: `User with email ${req.body.email} already exists!`});
  }
  const newUser = new User({
    name: req.body.name,
    email: req.body.email,
    isAdmin: false,
    password: bcrypt.hashSync(req.body.password, 8)
  });

  const createdUser = await newUser.save();

  res.status(200).send({
    _id: createdUser._id,
    name: createdUser.name,
    emai: createdUser.email,
    isAdmin: false,
    token: generateToken(createdUser)
  })
})

export default userRouter;