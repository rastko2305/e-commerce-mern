const data =[
    {
      name: 'Superman T Shirt',
      category: 'Shirts',
      image: '/images/p1.jpg',
      price: '56.01',
      brand: 'Adidas',
      rating: 4.5,
      numberOfReviews: 10,
      productproduct: 'Very cool T shirt',
      status: "In stock",
      numberOfItemsInStock: 10
    },
    {
      name: 'Military Shirt',
      category: 'Shirts',
      image: '/images/p2.jpeg',
      price: "230.99",
      brand: 'Adidas',
      rating: 3.5,
      numberOfReviews: 11,
      productDescription: 'Very cool',
      status: "In stock",
      numberOfItemsInStock: 7
    },
    {
      name: 'Military T Shirt',
      category: 'Shirts',
      image: '/images/p3.jpg',
      price: "56.99",
      brand: 'Nike',
      rating: 1.5,
      numberOfReviews: 41,
      productDescription: 'Very cool item',
      status: "In stock",
      numberOfItemsInStock: 977
    },
    {
      name: 'Coolest T Shirt',
      category: 'Shirts',
      image: '/images/p4.jpg',
      price: "30.00",
      brand: 'pumma',
      rating: 3.5,
      numberOfReviews: 21,
      productDescription: 'Very cool item',
      status: "In stock",
      numberOfItemsInStock: 9
    },
    {
      name: 'Tesla T Shirt',
      category: 'Shirts',
      image: '/images/p5.jpeg',
      price: "30.99",
      brand: 'Yumco',
      rating: 5.0,
      numberOfReviews: 44,
      productDescription: 'Very cool item',
      status: "In Stock",
      numberOfItemsInStock: 67
    },
    {
      name: 'Very Cool T Shirt',
      category: 'Shirts',
      image: '/images/p6.png',
      price: "9.99",
      brand: 'Nike',
      rating: 1.5,
      numberOfReviews: 100,
      productDescription: 'Very cool item',
      status: "Not Available",
      numberOfItemsInStock: 0
    },
    {
      name: 'Not Cool T Shirt 3',
      category: 'Shirts',
      image: '/images/p4.jpg',
      price: "30.00",
      brand: 'pumma',
      rating: 1.0,
      numberOfReviews: 21,
      productDescription: 'Very cool item',
      status: "In stock",
      numberOfItemsInStock: 9
    },
    {
      name: 'Nikola Tesla T Shirt ',
      category: 'Shirts',
      image: '/images/p5.jpeg',
      price: "30.99",
      brand: 'Yumco',
      rating: 5.0,
      numberOfReviews: 44,
      productDescription: 'Very cool item',
      status: "Not Available",
      numberOfItemsInStock: 0
    },
    {
      name: 'Very Coolest T Shirt',
      category: 'Shirts',
      image: '/images/p6.png',
      price: "9.99",
      brand: 'Nike',
      rating: 1.5,
      numberOfReviews: 47,
      productDescription: 'Very cool item',
      status: "Not Available",
      numberOfItemsInStock: 9
    },
    {
      name: 'Hey Shirt',
      category: 'Shirts',
      image: '/images/p6.png',
      price: "9.99",
      brand: 'Nike',
      rating: 1.5,
      numberOfReviews: 47,
      productDescription: 'Very cool item',
      status: "Not Available",
      numberOfItemsInStock: 9
    }
  ];

export default data;