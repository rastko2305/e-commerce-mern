import mongoose from "mongoose";

const productSchema = new mongoose.Schema(
  {
    name: { type: String, required: true, unique: true },
    image: { type: String, required: true },
    brand: { type: String, required: true },
    category: { type: String, required: true },
    productDescription: { type: String },
    price: { type: Number, required: true },
    numberOfItemsInStock: { type: Number, required: true },
    rating: { type: Number, required: true },
    numberOfReviews: { type: Number, required: true }
  },
  {
    timestamps: true,
  }
);
const Product = mongoose.model("Product", productSchema);

export default Product;